import plotly.express as px
import pandas as pd

def plot_scatter_map(df_filtrado, columna_mapa):
    color_discrete_map = px.colors.qualitative.Plotly
    fig = px.scatter_mapbox(df_filtrado,
                            lat="LAT_DECIMAL",
                            lon="LON_DECIMAL",
                            color=columna_mapa,
                            color_discrete_sequence=color_discrete_map,
                            hover_name="Municipio",
                            center={"lat": 23.6345, "lon": -102.5528},
                            hover_data={"LAT_DECIMAL": False, "LON_DECIMAL": False, "Estado": True, columna_mapa: True, "DiasRespuesta": True},
                            zoom=4.5,
                            height=650)
    
    conteo_filtro = df_filtrado[columna_mapa].value_counts().reset_index()
    conteo_filtro.columns = [columna_mapa, 'Count']
    fig_bar = px.bar(conteo_filtro, x=columna_mapa, y='Count', title=f'Conteo por {columna_mapa}',
                     color=columna_mapa, color_discrete_sequence=color_discrete_map)
    return fig, fig_bar

def plot_density_map(df_filtrado):
    color_continuous_map = px.colors.sequential.Viridis
    fig = px.density_mapbox(df_filtrado,
                            lat="LAT_DECIMAL",
                            lon="LON_DECIMAL",
                            z='CantidadSolicitudes',
                            radius=10,
                            hover_name="Municipio",
                            hover_data={"LAT_DECIMAL": False, "LON_DECIMAL": False, "Estado": True, 'CantidadSolicitudes': True},
                            zoom=4.5,
                            center={"lat": 23.6345, "lon": -102.5528},
                            height=650,
                            color_continuous_scale=color_continuous_map)
    
    conteo_municipio = df_filtrado['Municipio'].value_counts().reset_index()
    conteo_municipio.columns = ['Municipio', 'Count']
    fig_bar = px.bar(conteo_municipio, x='Municipio', y='Count', title='Conteo por Municipio',
                     color='Count', color_continuous_scale=color_continuous_map)
    return fig, fig_bar

def plot_choropleth_map(df_estado, geojson, estilo_mapa):
    fig = px.choropleth_mapbox(df_estado,
                               geojson=geojson,
                               locations='Estado',
                               featureidkey='properties.name',
                               color='Quantiles',
                               hover_name='Estado',
                               hover_data={'CantidadSolicitudes': True, 'Quantiles': True},
                               zoom=4.5,
                               center={"lat": 23.6345, "lon": -102.5528},
                               mapbox_style=estilo_mapa,
                               height=650,
                               color_continuous_scale="Viridis")

    conteo_estado = df_estado[['Estado', 'CantidadSolicitudes', 'Quantiles']]
    conteo_estado.columns = ['Estado', 'Count', 'Quantiles']
    conteo_estado = conteo_estado.sort_values(by='Count', ascending=False)
    fig_bar = px.bar(conteo_estado, x='Estado', y='Count', title='Conteo por Estado',
                     color='Quantiles', color_continuous_scale="Viridis")
    return fig, fig_bar
