import streamlit as st
import pandas as pd
import json
from data_loader import load_data
from map_plotter import plot_scatter_map, plot_density_map, plot_choropleth_map

# Utilizar caché para cargar datos
@st.cache_data
def load_cached_data(csv_file, geojson_file):
    df = pd.read_csv(csv_file)
    with open(geojson_file) as f:
        geojson = json.load(f)
    return df, geojson

def mapas_solicitudes():
    st.markdown("""
    ### Visualización Geoespacial de Solicitudes de Información

    Este apartado permite crear visualización geoespacial de las solicitudes de información, permitiendo crear 3 tipos de mapas diferentes:
    - **Mapa coroplético**: Muestra la distribución de las solicitudes por estado.
    - **Mapa de dispersión**: Permite ver la distribución de las solicitudes en base a columnas específicas como 'Sector', 'Respuesta', 'Estatus', 'TipoSolicitud', 'MedioEntrada', 'MedioEntrega', 'Dependencia'.
    - **Mapa de calor**: Visualiza la densidad de las solicitudes en el mapa.
    """)
    
    # Cargar los datos con caché
    df, geojson = load_cached_data("merge_mapa.csv", "mexicoHigh.json")

    # Filtro por año de solicitud
    anio_solicitud = st.sidebar.selectbox("Selecciona Año de Solicitud", options=df["AnioSolicitud"].unique())

    # Filtrar el DataFrame por año de solicitud
    df_filtrado = df[df["AnioSolicitud"] == anio_solicitud]

    if df_filtrado.empty:
        st.warning("No hay datos disponibles para el año seleccionado.")
    else:
        df_estado = df_filtrado.groupby('Estado').size().reset_index(name='CantidadSolicitudes')
        df_estado['Quantiles'] = pd.qcut(df_estado['CantidadSolicitudes'], q=5, labels=False)

        tipo_mapa = st.sidebar.selectbox("Selecciona el Tipo de Mapa", options=['Mapa de dispersión', 'Mapa de calor', 'Mapa coroplético'])
        estilo_mapa = st.sidebar.selectbox("Selecciona el Estilo de Mapa", options=['carto-darkmatter', 'open-street-map', 'carto-positron'])

        titulo = f"{tipo_mapa.replace(' ', ' ')} de solicitudes durante el año {anio_solicitud}"
        st.title(titulo)

        if tipo_mapa == 'Mapa de dispersión':
            columna_mapa = st.sidebar.selectbox("Selecciona la Columna para el Mapa", options=['Sector', 'Respuesta', 'Estatus', 'TipoSolicitud', 'MedioEntrada', 'MedioEntrega', 'Dependencia'])
            fig, fig_bar = plot_scatter_map(df_filtrado, columna_mapa)
        elif tipo_mapa == 'Mapa de calor':
            df_filtrado['CantidadSolicitudes'] = 1
            fig, fig_bar = plot_density_map(df_filtrado)
        elif tipo_mapa == 'Mapa coroplético':
            fig, fig_bar = plot_choropleth_map(df_estado, geojson, estilo_mapa)

        fig.update_layout(mapbox_style=estilo_mapa)
        fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

        st.plotly_chart(fig, use_container_width=True)
        st.plotly_chart(fig_bar, use_container_width=True)

if __name__ == "__main__":
    mapas_solicitudes()
