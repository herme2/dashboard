import pandas as pd
import json

def load_data(csv_path, json_path):
    df = pd.read_csv(csv_path)
    with open(json_path) as response:
        geojson = json.load(response)
    return df, geojson
